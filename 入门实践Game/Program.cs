﻿using System;

namespace 入门实践Game
{
    class Program
    {
        static void Main(string[] args)
        {
            #region 控制台基础设置
            int width = 50;
            int height = 30;
            // 隐藏光标
            Console.CursorVisible = false;
            // 设置舞台(控制台)大小
            Console.SetWindowSize(width, height);
            // 设置缓存区大小
            Console.SetBufferSize(width, height);
            #endregion

            #region 多个场景
            int nowSceneId = 1;
            string gameOverInfo = "";
            while (true)
            {
                // 通过ID表示场景
                switch (nowSceneId) 
                {
                    case 1:
                        #region 开始游戏逻辑
                        Console.Clear();
                        Console.SetCursorPosition(width / 2 - 6, 8);
                        Console.Write("骑士营救公主");
                        int nowSelect = 1;

                        // 输入
                        while (true)
                        {
                            bool isQuitWhile = false;
                            // 显示内容，检测输入
                            Console.SetCursorPosition(width / 2 - 4, 13);
                            Console.ForegroundColor = nowSelect == 1 ? ConsoleColor.Red : Console.ForegroundColor = ConsoleColor.White;
                            Console.Write("开始游戏");
                            Console.SetCursorPosition(width / 2 - 4, 15);
                            Console.ForegroundColor = nowSelect == 2 ? ConsoleColor.Red : Console.ForegroundColor = ConsoleColor.White;
                            Console.Write("退出游戏");
                            char input = Console.ReadKey(true).KeyChar;
                            switch (input)
                            {
                                case 'W':
                                case 'w':
                                    --nowSelect;
                                    if (nowSelect < 1)
                                    {
                                        nowSelect = 1;
                                    }
                                    break;
                                case 'S':
                                case 's':
                                    ++nowSelect;
                                    if (nowSelect > 2)
                                    {
                                        nowSelect = 2;
                                    }
                                    break;
                                case 'J':
                                case 'j':
                                    if (nowSelect == 1)
                                    {
                                        nowSceneId = 2;
                                        isQuitWhile = true;
                                    }
                                    else
                                    {
                                        Environment.Exit(0);
                                    }
                                    break;
                            }
                            if (isQuitWhile)
                            {
                                break;
                            }
                        }
                        #endregion
                        break;
                    case 2:
                        Console.Clear();
                        #region 不变的红墙
                        Console.ForegroundColor = ConsoleColor.Red;
                        for (int i = 0; i < width; i += 2)
                        {
                            // 上面的墙
                            Console.SetCursorPosition(i, 0);
                            Console.Write("■");
                            // 下面的墙
                            Console.SetCursorPosition(i, height - 1);
                            Console.Write("■");
                            // 中间的墙
                            Console.SetCursorPosition(i, height - 6);
                            Console.Write("■");
                        }

                        for (int i = 0; i < height; i++)
                        {
                            // 左边的墙
                            Console.SetCursorPosition(0, i);
                            Console.Write("■");
                            // 右边的墙
                            Console.SetCursorPosition(width - 2, i);
                            Console.Write("■");
                        }
                        #endregion

                        #region Boss属性
                        int BossX = 24;
                        int BossY = 15;
                        int bossAtkMin = 7;
                        int bossAtkMax = 13;
                        int bossHp = 100;
                        string bossIcon = "■";
                        ConsoleColor bossColor = ConsoleColor.Green;
                        #endregion

                        #region 玩家属性
                        int playerX = 4;
                        int playerY = 5;
                        int playerAtkMin = 8;
                        int playerAtkMax = 12;
                        int playerHp = 100;
                        string playerIcon = "○";
                        ConsoleColor playerColor = ConsoleColor.Yellow;
                        // 玩家输入内容，提高性能
                        char playerInput;
                        #endregion

                        #region 公主相关
                        int princessX = 24;
                        int princessY = 5;
                        string princessIcon = "★";
                        ConsoleColor princessColor = ConsoleColor.Blue;
                        #endregion

                        #region 玩家战斗相关
                        // 战斗状态
                        bool isFight = false;
                        bool isOver = false;
                        #endregion
                        #region 真正游戏逻辑
                        while (true)
                        {
                            if (bossHp > 0)
                            {
                                // 绘制Boss图标
                                Console.SetCursorPosition(BossX, BossY);
                                Console.ForegroundColor = bossColor;
                                Console.Write(bossIcon);
                            }
                            else
                            {
                                Console.SetCursorPosition(princessX, princessY);
                                Console.ForegroundColor = princessColor;
                                Console.Write(princessIcon);
                            }

                            Console.SetCursorPosition(playerX, playerY);
                            Console.ForegroundColor = playerColor;
                            Console.Write(playerIcon);

                            playerInput = Console.ReadKey(true).KeyChar;
                            if (isFight)
                            {
                                if ( playerInput == 'J' || playerInput == 'j' )
                                {
                                    if (playerHp <= 0)
                                    {
                                        // 游戏结束
                                        nowSceneId = 3;
                                        break;
                                    }
                                    else if (bossHp <= 0)
                                    {
                                        Console.SetCursorPosition(BossX, BossY);
                                        Console.Write("  ");
                                        isFight = false;
                                    }
                                    else
                                    {
                                        // 玩家打怪物
                                        Random r = new Random();
                                        int atk = r.Next(playerAtkMin, playerAtkMax);
                                        // 怪物掉血
                                        bossHp -= atk;
                                        // 打印信息
                                        Console.ForegroundColor = ConsoleColor.Green;
                                        // 擦除这一行
                                        Console.SetCursorPosition(2, height - 4);
                                        Console.Write("                                         ");
                                        Console.SetCursorPosition(2, height - 4);
                                        Console.Write($"你对Boss造成了{atk}点伤害，boss剩余血量为{bossHp}");
                                        // 怪物打玩家
                                        if (bossHp > 0)
                                        {
                                            atk = r.Next(bossAtkMin, bossAtkMax);
                                            playerHp -= atk;
                                            // 打印信息
                                            Console.ForegroundColor = ConsoleColor.Yellow;
                                            // 擦除这一行
                                            Console.SetCursorPosition(2, height - 3);
                                            Console.Write("                                         ");
                                            if (playerHp <= 0)
                                            {
                                                Console.SetCursorPosition(2, height - 3);
                                                Console.Write("很遗憾，你未能通过Boss的试炼");
                                            }
                                            else
                                            {
                                                Console.SetCursorPosition(2, height - 3);
                                                Console.Write($"Boss对你造成了{atk}点伤害，你的剩余血量为{playerHp}");
                                            }
                                        }
                                        else
                                        {
                                            Console.SetCursorPosition(2, height - 5);
                                            Console.Write("                                         ");
                                            Console.SetCursorPosition(2, height - 4);
                                            Console.Write("                                         ");
                                            Console.SetCursorPosition(2, height - 3);
                                            Console.Write("                                         ");
                                            Console.SetCursorPosition(2, height - 5);
                                            Console.Write("你战胜了boss，快去营救公主");
                                            Console.SetCursorPosition(2, height - 4);
                                            Console.Write("前往公主身边按J键继续");
                                        }
                                    }  
                                }
                            }
                            else
                            {
                                #region 玩家移动相关
                                // 非战斗状态
                                // 擦除
                                Console.SetCursorPosition(playerX, playerY);
                                Console.Write("  ");
                                // 改位置
                                switch (playerInput)
                                {
                                    case 'W':
                                    case 'w':
                                        --playerY;
                                        if (playerY < 1)
                                        {
                                            playerY = 1;
                                        }
                                        //位置如果和boss重合了 并且boss没有死
                                        else if (playerX == BossX && playerY == BossY && bossHp > 0)
                                        {
                                            //拉回去
                                            ++playerY;
                                        }
                                        else if (playerX == princessX && playerY == princessY && bossHp <= 0)
                                        {
                                            //拉回去
                                            ++playerY;
                                        }
                                        break;
                                    case 'A':
                                    case 'a':
                                        playerX -= 2;
                                        if (playerX < 2)
                                        {
                                            playerX = 2;
                                        }
                                        else if (playerX == BossX && playerY == BossY && bossHp > 0)
                                        {
                                            //拉回去
                                            playerX += 2;
                                        }
                                        else if (playerX == princessX && playerY == princessY && bossHp <= 0)
                                        {
                                            //拉回去
                                            playerX += 2;
                                        }
                                        break;
                                    case 'S':
                                    case 's':
                                        ++playerY;
                                        if (playerY > height - 7)
                                        {
                                            playerY = height - 7;
                                        }
                                        else if (playerX == BossX && playerY == BossY && bossHp > 0)
                                        {
                                            //拉回去
                                            --playerY;
                                        }
                                        else if (playerX == princessX && playerY == princessY && bossHp <= 0)
                                        {
                                            //拉回去
                                            --playerY;
                                        }
                                        break;
                                    case 'D':
                                    case 'd':
                                        playerX += 2;
                                        if (playerX > width - 4)
                                        {
                                            playerX = width - 4;
                                        }
                                        else if (playerX == BossX && playerY == BossY && bossHp > 0)
                                        {
                                            //拉回去
                                            playerX -= 2;
                                        }
                                        else if (playerX == princessX && playerY == princessY && bossHp <= 0)
                                        {
                                            //拉回去
                                            playerX -= 2;
                                        }
                                        break;
                                    case 'J':
                                    case 'j':
                                        // 开始战斗
                                        if ((playerX == BossX && playerY == BossY - 1 ||
                                            playerX == BossX && playerY == BossY + 1 ||
                                            playerX == BossX - 2 && playerY == BossY ||
                                            playerX == BossX + 2 && playerY == BossY) && bossHp > 0)
                                        {
                                            // 可以开始战斗
                                            isFight = true;
                                            Console.SetCursorPosition(2, height - 5);
                                            Console.ForegroundColor = ConsoleColor.White;
                                            Console.Write("开始和Boss战斗了，按J键继续");
                                            Console.SetCursorPosition(2, height - 4);
                                            Console.Write($"玩家当前血量为：{playerHp}");
                                            Console.SetCursorPosition(2, height - 3);
                                            Console.Write($"Boss当前血量为：{bossHp}");
                                        }
                                        else if ((playerX == princessX && playerY == princessY - 1 ||
                                           playerX == princessX && playerY == princessY + 1 ||
                                           playerX == princessX - 2 && playerY == princessY ||
                                           playerX == princessX + 2 && playerY == princessY) && bossHp <= 0)
                                        {
                                            //改变 场景ID
                                            nowSceneId = 3;
                                            //跳出 游戏界面的while循环 回到主循环
                                            isOver = true;
                                        }
                                        break;
                                }
                                #endregion
                            }
                            if (isOver)
                            {
                                //就是和while配对
                                break;
                            }
                        }
                        #endregion
                        break;
                    case 3:
                        Console.Clear();
                        #region 9 结束场景逻辑
                        //标题的显示
                        Console.SetCursorPosition(width / 2 - 4, 5);
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.Write("GameOver");
                        //可变内容的显示 根据失败或者 成功显示的内容不一样
                        Console.SetCursorPosition(width / 2 - 4, 7);
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.Write(gameOverInfo);

                        int nowSelEndIndex = 0;
                        while (true)
                        {
                            bool isQuitEndWhile = false;

                            Console.SetCursorPosition(width / 2 - 6, 9);
                            Console.ForegroundColor = nowSelEndIndex == 0 ? ConsoleColor.Red : ConsoleColor.White;
                            Console.Write("回到开始界面");
                            Console.SetCursorPosition(width / 2 - 4, 11);
                            Console.ForegroundColor = nowSelEndIndex == 1 ? ConsoleColor.Red : ConsoleColor.White;
                            Console.Write("退出游戏");

                            char input = Console.ReadKey(true).KeyChar;

                            switch (input)
                            {
                                case 'W':
                                case 'w':
                                    --nowSelEndIndex;
                                    if (nowSelEndIndex < 0)
                                    {
                                        nowSelEndIndex = 0;
                                    }
                                    break;
                                case 'S':
                                case 's':
                                    ++nowSelEndIndex;
                                    if (nowSelEndIndex > 1)
                                    {
                                        nowSelEndIndex = 1;
                                    }
                                    break;
                                case 'J':
                                case 'j':
                                    if (nowSelEndIndex == 0)
                                    {
                                        nowSceneId = 1;
                                        isQuitEndWhile = true;
                                    }
                                    else
                                    {
                                        Environment.Exit(0);
                                    }
                                    break;
                            }

                            //为了 从switch中跳出上一层的 while循环 加的标识
                            if (isQuitEndWhile)
                            {
                                break;
                            }
                        }
                        #endregion
                        break;
                }
            }
            #endregion
        }
    }
}
